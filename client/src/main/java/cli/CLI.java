package cli;

import core.model.Client;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.RestTemplate;
import web.dto.ClientDto;
import web.dto.ClientListDto;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class CLI {
    private RestTemplate restTemplate;

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("cli.config");

        CLI cli = new CLI();
        cli.restTemplate = context.getBean(RestTemplate.class);
        cli.run();
    }

    private void printUsage() {
        String usage =
                "addClient <name>,<serial>,<age> : Add a client\n" +
                        "printClients : Print all clients\n" +
                        "updateClient <name>,<serial>,<age>,<id> : Update the client with the given id\n" +
                        "deleteClient <id> : delete the client with the given id\n";

        System.out.println(usage);
    }

    private void execute(String command, List<String> args) throws InvalidParameterException {
        switch (command) {

            case ("addClient"): {
                String name, serial;
                int age;

                if (args.size() != 3) {
                    throw new InvalidParameterException("Invalid number of parameters.  Needed 3, got " + args.size());
                }

                name = args.get(0);
                serial = args.get(1);
                try {
                    age = Integer.parseInt(args.get(2));
                } catch (NumberFormatException nfe) {
                    throw new InvalidParameterException("Invalid age value. Must be integer, not " + args.get(2));
                }

                //controller.addClient(new Client(name, age, email));
                // save the client in the db:
                ClientDto newClient = ClientDto.builder()
                        .firstName(name)
                        .lastName(name + "-last")
                        .serialNumber(serial)
                        .build();
                restTemplate.postForObject("http://localhost:8080/client/", newClient, ClientDto.class);
                break;
            }

            case ("deleteClient"): {
                if (args.size() != 1) {
                    throw new InvalidParameterException("Invalid number of parameters.  Needed 1, got " + args.size());
                }

                int id;
                try {
                    id = Integer.parseInt(args.get(0).replace(" ", ""));
                } catch (NumberFormatException nfe) {
                    throw new InvalidParameterException("Invalid id value.  Must be integer, not " + args.get(0));
                }

                restTemplate.delete("http://localhost:8080/client/{id}", id);
                break;
            }


            case ("updateClient"): {
                String name, serial;
                Long id;

                if (args.size() != 4) {
                    throw new InvalidParameterException("Invalid number of parameters.  Needed 4, got " + args.size());
                }

                name = args.get(0);
                serial = args.get(1);

                try {
                    id = Long.parseLong(args.get(3));
                } catch (NumberFormatException nfe) {
                    throw new InvalidParameterException("Invalid id value. Must be integer, not " + args.get(3));
                }

                Client client = new Client(name, name, serial);
                client.setId(id);

                restTemplate.put("http://localhost:8080/client/", client);
                break;
            }

            case ("printClients"): {
                ClientListDto all = restTemplate.getForObject("http://localhost:8080/clients", ClientListDto.class);

                all.getClients().forEach(System.out::println);

                break;
            }

            case ("help"): {
                printUsage();
                break;
            }

            case ("clear"): {
                for (int i = 0; i < 30; i++) {
                    System.out.println();
                }
                break;
            }
            default:
                System.out.println("Invalid command");
        }
    }

    public void run() {
        boolean quitting = false;
        String line, command;
        List<String> args;
        Scanner scanner = new Scanner(System.in);

        while (!quitting) {
            System.out.println(">>> ");
            line = scanner.nextLine();

            if (line.length() == 0)
                //empty line, loop again
                continue;

            command = line.split(" ")[0];
            // get the args, everything except for the first elem. which is the command

            args = Arrays.asList(line.replace(command + " ", "").split(","));

            if (command.equals("exit")) {
                quitting = true;
                continue;
            }
            try {
                execute(command, args);
            } catch (InvalidParameterException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
