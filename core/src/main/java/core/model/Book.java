package core.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Book extends BaseEntity<Long> {
    private String title;
    private String author;
    private int rating;

    @Override
    @OneToMany
    @JoinColumn(name = "bookId")
    public Long getId() {
        return super.getId();
    }
}
