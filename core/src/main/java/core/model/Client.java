package core.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Client extends BaseEntity<Long>{
    private String serialNumber;
    private String firstName;
    private String lastName;

    @Override
    @OneToMany
    @JoinColumn(name = "clientId")
    public Long getId() {
        return super.getId();
    }
}
