package core.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Transaction extends BaseEntity<Long> {
    private Long bookId;
    private Long clientId;
    private String notes;

    @ManyToOne
    @JoinColumn(name = "bookId")
    public Long getBookId(){
        return this.bookId;
    }

    @ManyToOne
    @JoinColumn(name = "clientId")
    public Long getClientId(){
        return this.clientId;
    }


}
