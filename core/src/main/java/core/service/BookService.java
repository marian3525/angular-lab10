package core.service;

import core.model.Book;
import core.model.Client;

import java.util.List;

public interface BookService {
    List<Book> findAll();

    Book findById(Long id);

    List<Book> findByTitle(String title);

    void save(Book book);

    Book update(Long id, Book b);

    void delete(Long id);
}
