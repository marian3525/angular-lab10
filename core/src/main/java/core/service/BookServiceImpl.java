package core.service;

import core.model.Book;
import core.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    private static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    BookRepository repository;

    @Override
    public List<Book> findAll() {
        log.trace("findAll --- method entered");

        return repository.findAll();
    }

    @Override
    public Book findById(Long id) {
        log.trace("findById --- method entered");

        Optional<Book> c = repository.findById(id);

        return c.orElse(null);
    }

    @Override
    public List<Book> findByTitle(String title){
        log.trace("findByFirstName --- method entered");

        return repository.findByTitle(title);
    }

    @Override
    public void save(Book book) {
        log.trace("save --- method entered");

        repository.save(book);
    }

    @Override
    public Book update(Long id, Book c) {
        log.trace("update --- method entered");

        Optional<Book> prev = repository.findById(id);

        if(prev.isPresent()) {
            prev.get().setTitle(c.getTitle());
            prev.get().setAuthor(c.getAuthor());
            prev.get().setRating(c.getRating());
            repository.save(prev.get());

            return prev.get();
        }

        if(null != c) {
            repository.save(c);

            return c;
        }

        return null;
    }

    @Override
    public void delete(Long id) {
        log.trace("delete --- method entered");

        repository.deleteById(id);
    }
}
