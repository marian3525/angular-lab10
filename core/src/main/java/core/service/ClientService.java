package core.service;

import core.model.Client;

import java.util.HashMap;
import java.util.List;

public interface ClientService {
    List<Client> findAll();

    Client findById(Long id);

    List<Client> findByFirstName(String firstName);

    void save(Client client);

    Client update(Long id, Client c);

    void delete(Long id);
}
