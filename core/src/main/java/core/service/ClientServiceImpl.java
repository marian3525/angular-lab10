package core.service;

import core.model.Client;
import core.repository.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {
    private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    ClientRepository repository;

    @Override
    public List<Client> findAll() {
        log.trace("findAll --- method entered");

        return repository.findAll();
    }

    @Override
    public Client findById(Long id) {
        log.trace("findById --- method entered");

        Optional<Client> c = repository.findById(id);

        return c.orElse(null);
    }

    @Override
    public List<Client> findByFirstName(String firstName){
        log.trace("findByFirstName --- method entered");

        return repository.findByFirstName(firstName);
    }

    @Override
    public void save(Client client) {
        log.trace("save --- method entered");

        repository.save(client);
    }

    @Override
    public Client update(Long id, Client c) {
        log.trace("update --- method entered");

        Optional<Client> prev = repository.findById(id);

        if(prev.isPresent()) {
            prev.get().setFirstName(c.getFirstName());
            prev.get().setLastName(c.getLastName());
            prev.get().setSerialNumber(c.getSerialNumber());
            repository.save(prev.get());

            return prev.get();
        }

        if(null != c) {
            repository.save(c);

            return c;
        }

        return null;
    }

    @Override
    public void delete(Long id) {
        log.trace("delete --- method entered");

        repository.deleteById(id);
    }
}
