package core.service;

import core.model.Transaction;

import java.util.List;

public interface TransactionService {
    List<Transaction> findAll();

    Transaction findById(Long id);

    void save(Transaction transaction);

    Transaction update(Long id, Transaction t);

    void delete(Long id);
}
