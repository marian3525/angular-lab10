package core.service;

import core.model.Transaction;
import core.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class TransactionServiceImpl implements TransactionService {
    private static final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Autowired
    TransactionRepository repository;

    @Override
    public List<Transaction> findAll() {
        log.trace("findAll --- method entered");

        return repository.findAll();
    }

    @Override
    public Transaction findById(Long id) {
        log.trace("findById --- method entered");

        Optional<Transaction> t = repository.findById(id);

        return t.orElse(null);
    }

    @Override
    public void save(Transaction transaction) {
        log.trace("save --- method entered");

        repository.save(transaction);
    }

    @Override
    public Transaction update(Long id, Transaction t) {
        log.trace("update --- method entered");

        Optional<Transaction> prev = repository.findById(id);

        if(prev.isPresent()) {
            prev.get().setBookId(t.getBookId());
            prev.get().setClientId(t.getClientId());
            repository.save(prev.get());

            return prev.get();
        }

        if(null != t) {
            repository.save(t);

            return t;
        }

        return null;
    }

    @Override
    public void delete(Long id) {
        log.trace("delete --- method entered");

        repository.deleteById(id);
    }
}
