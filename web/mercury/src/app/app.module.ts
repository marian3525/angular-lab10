import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ClientsDashboardComponent } from './clients-dashboard/clients-dashboard.component';
import { BooksDashboardComponent} from './books-dashboard/books-dashboard.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { BooksListComponent} from "./book-list/book-list.component";
import { ClientRegisterComponent } from './client-register/client-register.component';
import { BookRegisterComponent} from "./book-register/book-register.component";
import { ClientSearchComponent } from './client-search/client-search.component';
import { BookSearchComponent} from './book-search/book-search.component';

import { HttpClientModule } from "@angular/common/http";
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from "@angular/router";
import { HomeComponent } from './home/home.component';

import { FormsModule } from "@angular/forms";

import {TransactionDashboardComponent} from "./transaction-dashboard/transaction-dashboard.component";
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { TransactionRegisterComponent } from './transaction-register/transaction-register.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientsDashboardComponent,
    BooksDashboardComponent,
    TransactionDashboardComponent,
    ClientsListComponent,
    BooksListComponent,
    ClientRegisterComponent,
    BookRegisterComponent,
    NavbarComponent,
    HomeComponent,
    ClientSearchComponent,
    BookSearchComponent,
    TransactionListComponent,
    TransactionRegisterComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },

      { path: 'clients', component:ClientsDashboardComponent },
      { path: 'client/register', component: ClientRegisterComponent },
      { path: 'search/client/:firstName', component: ClientSearchComponent },

      { path: 'books', component: BooksDashboardComponent },
      { path: 'books/register', component: BookRegisterComponent },
      { path: 'search/book/:title', component: BookSearchComponent},

      { path: 'transactions', component: TransactionDashboardComponent },
      { path: 'transactions/register', component: TransactionRegisterComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent, NavbarComponent]
})
export class AppModule { }
