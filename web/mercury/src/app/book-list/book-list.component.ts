import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Book} from "../shared/book";
import {HttpUtils} from "../shared/httpUtils";
import {Validator} from "../shared/validator";

@Component({
  selector: 'app-books-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BooksListComponent implements OnInit {

  books = [];
  book: Book = new Book(0, "default", "default", 0);

  constructor(private http : HttpClient) {
    console.log("populating book list");
    this.books = HttpUtils.getBookList(http);
  }

  deleteBook(book) {
    HttpUtils.deleteBook(this.http, book);

    let index = this.books.indexOf(book);
    this.books.splice(index, 1);
  }

  openModal(book: Book) {
    this.book = book;
  }

  updateBook(title: HTMLInputElement, author: HTMLInputElement, rating: HTMLInputElement) {

    try {
      Validator.validateBookByParams(this.book.id, title.value, author.value, Number.parseInt(rating.value));

      let updatedBook = HttpUtils.updateBook(this.http, this.book, title.value, author.value, rating.value);

      let index = this.books.indexOf(this.book);
      this.books[index].serialNumber = updatedBook.title;
      this.books[index].firstName = updatedBook.author;
      this.books[index].lastName = updatedBook.rating;
    }
    catch(valException) {
        alert(valException.getMessage());
    }
  }

  ngOnInit() {
  }

}
