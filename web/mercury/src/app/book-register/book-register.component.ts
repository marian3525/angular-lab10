import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Location } from '@angular/common';
import {HttpUtils} from "../shared/httpUtils";
import {Validator} from "../shared/validator";

@Component({
  selector: 'app-book-register',
  templateUrl: './book-register.component.html',
  styleUrls: ['./book-register.component.css']
})
export class BookRegisterComponent implements OnInit {


  constructor(private http: HttpClient, private location: Location) { }

  ngOnInit() {
  }

  createBook(title: HTMLInputElement, author: HTMLInputElement, rating: HTMLInputElement) {
    try {
      Validator.validateBookByParams(0, title.value, author.value, Number.parseInt(rating.value));
      HttpUtils.createBook(this.http, title.value, author.value, Number.parseInt(rating.value));
    }
    catch(valException) {
      alert(valException.getMessage());
    }
  }

}
