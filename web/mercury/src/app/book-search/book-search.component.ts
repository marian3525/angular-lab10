import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpUtils} from "../shared/httpUtils";

@Component({
  selector: 'app-book-search',
  templateUrl: './book-search.component.html',
  styleUrls: ['./book-search.component.css']
})

export class BookSearchComponent implements OnInit {

  books = [];
  id = 0;

  constructor(private http: HttpClient,
              private _Activatedroute: ActivatedRoute,
              private router: Router) {

    this.getBooks(this.id);

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  getBooks(title) {
    this.books = HttpUtils.searchBookByTitle(this.http, title);
  }

  ngOnInit() {
    this.id = this._Activatedroute.snapshot.params['title'];
    this.getBooks(this.id);
  }

}
