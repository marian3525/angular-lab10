import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Location } from '@angular/common';
import {first, windowWhen} from "rxjs/operators";
import {Validator} from "../shared/validator";
import {HttpUtils} from "../shared/httpUtils";

@Component({
  selector: 'app-client-register',
  templateUrl: './client-register.component.html',
  styleUrls: ['./client-register.component.css']
})
export class ClientRegisterComponent implements OnInit {


  constructor(private http: HttpClient, private location: Location) { }

  ngOnInit() {
  }

  createClient(firstName: HTMLInputElement, lastName: HTMLInputElement, serialNumber: HTMLInputElement) {

    try {
      Validator.validateClientByParams(0, firstName.value, lastName.value, serialNumber.value);
      HttpUtils.createClient(this.http, firstName.value, lastName.value, serialNumber.value);
    }
    catch (valException) {
      console.log(valException.getMessage());
    }

  }

}
