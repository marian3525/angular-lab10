import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {Client} from "../shared/client";
import {HttpUtils} from "../shared/httpUtils";

@Component({
  selector: 'app-client-search',
  templateUrl: './client-search.component.html',
  styleUrls: ['./client-search.component.css']
})
export class ClientSearchComponent implements OnInit {

  clients = [];
  id = 0;

  constructor(private http: HttpClient,
              private _Activatedroute: ActivatedRoute,
              private router: Router) {
    this.getClients(this.id);
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  getClients(firstName) {
    this.clients = HttpUtils.searchClientByFirstName(this.http, firstName);
  }

  ngOnInit() {
    this.id = this._Activatedroute.snapshot.params['firstName'];
    this.getClients(this.id);
    console.log(this.id);
  }

}
