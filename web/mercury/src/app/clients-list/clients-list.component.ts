import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Client} from "../shared/client";
import {HttpUtils} from "../shared/httpUtils";
import {last} from "rxjs/operators";
import {Validator} from "../shared/validator";

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css']
})
export class ClientsListComponent implements OnInit {


  clients = [];
  client: Client = new Client(0, "default", "default", "default");

  constructor(private http : HttpClient) {
    this.clients = HttpUtils.getClientsList(http);
  }

  deleteClient(client) {
    HttpUtils.deleteClient(this.http, client);
    let index = this.clients.indexOf(client);
    this.clients.splice(index, 1);
  }

  openModal(client: Client) {
    this.client = client;
  }

  updateClient(firstName: HTMLInputElement, lastName: HTMLInputElement, serialNumber: HTMLInputElement) {
    try {
      Validator.validateClientByParams(0, firstName.value, lastName.value, serialNumber.value);
      let updatedClient = HttpUtils.updateClient(this.http, this.client, firstName.value, lastName.value, serialNumber.value);

      let index = this.clients.indexOf(this.client);
      this.clients[index].serialNumber = updatedClient.serialNumber;
      this.clients[index].firstName = updatedClient.firstName;
      this.clients[index].lastName = updatedClient.lastName;
    }
    catch(valException) {
      console.log(valException);
    }
  }

  ngOnInit() {
  }
}
