import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  searchInputClient: string;
  searchInputBook: string;

  clearInput() {
    this.searchInputClient = null;
    this.searchInputBook = null;
  }

  constructor() { }

  ngOnInit() {
  }

}
