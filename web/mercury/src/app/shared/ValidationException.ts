export class ValidationException {
  constructor(private readonly message: string) {
  }

  getMessage(): string {
    return this.message;
  }

}
