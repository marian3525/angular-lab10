export class Client {
  constructor (
    public id: number,
    public serialNumber: string,
    public firstName: string,
    public lastName: string
  ) {}
}
