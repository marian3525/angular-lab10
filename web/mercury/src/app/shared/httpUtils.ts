import {HttpClient} from "@angular/common/http";
import {Book} from "./book";
import {Client} from "./client";
import {Routing} from "./routing";
import {Transaction} from "./transaction";

export class HttpUtils {

  /**
   * Create a new book with the given params
   * @param http: HTTP client
   * @param title: title string
   * @param author: author string
   * @param rating: rating number
   */
  public static createBook(http: HttpClient, title: string, author: string, rating: number) {
    let body = {
      title: title,
      author: author,
      rating: rating
    };

    http.post(Routing.createBookURL, body)
      .subscribe(response => {
          window.location.href = '/books';
        },
        error => {
          confirm("An error occurred: " + error.toString());
        });
  }

  /**
   * Return a list containing all books
   * @param http: HTTPClient
   */
  public static getBookList(http: HttpClient): Book[] {
    let books = [];

    http.get(Routing.booksListURL)
      .subscribe(response => {
        for(let book of response['books']) {
          books.push(book);
        }
      },
          error => {
        console.log(error);
        return [];
      });
    console.log("books in getBookList()");
    console.log(books);

    return books;
  }

  /**
   * Delete the given book from the repo
   * @param http: HTTPClient
   * @param book: Book to be deleted
   */
  public static deleteBook(http: HttpClient, book: Book) {
    http.delete(Routing.bookURL + '/' + book.id)
      .subscribe(response => {
        console.log(response);
      });
  }

  /**
   * Update the given book using the new parameters and return it
   * @return the updated book
   * @param http HTTPClient
   * @param book Book to be updated
   * @param title New title
   * @param author New author
   * @param rating New rating
   */
  public static updateBook(http: HttpClient, book: Book, title: string, author: string, rating: string): Book {

    let body = {
      id: book.id,
      title: title,
      author: author,
      rating: rating
    };

    http.put(Routing.bookURL + '/', body)
      .subscribe(response => {
          console.log(response);
        },
        error => {
          console.log(error);
        });

    return new Book(book.id, title, author, Number.parseInt(rating));
  }

  /**
   * Search for a book by its title. Retuns book that have the given title
   * @param http HttpClient
   * @param title The title to look for
   */
  public static searchBookByTitle(http: HttpClient, title: string): Book[] {
    let books = [];

    http.get<Book[]>(Routing.searchBookURL + '?title=' + title)
      .subscribe(response => {
        for (let book of response) {
          books.push(book);
        }
      }, error => {
        confirm("An error occurred: " + error.toString());
      });

    return books;
  }

  /**
   * Create a new client with the given params
   * @param http HttpClient
   * @param firstName First name string
   * @param lastName Last name string
   * @param serial Serial string
   */
  public static createClient(http: HttpClient, firstName: string, lastName: string, serial: string) {
    let body = {
      serialNumber: serial,
      firstName: firstName,
      lastName: lastName
    };

    http.post(Routing.createClientURL, body)
      .subscribe(() => {
        // redirect to the list
          window.location.href = '/clients';
        },
        error => {
          confirm("An error occurred: " + error.toString());
        });
  }

  /**
   * Return all clients in the DB
   * @param http HTTPClient
   */
  public static getClientsList(http: HttpClient): Client[] {
    let clients = [];

    http.get(Routing.clientsListURL)
      .subscribe(response => {
        for(let client of response['clients']) {
          clients.push(client);
        }
      }, error => {
        console.log(error);
      });

    return clients;
  }

  /**
   * Delete a client
   * @param http HttpClient
   * @param client The client to be deleted
   */
  public static deleteClient(http: HttpClient, client: Client) {
    http.delete(Routing.clientURL + '/' + client.id)
      .subscribe(response => {
        console.log(response);
      });
  }

  /**
   * Update the given client using the new parameters and return it
   * @param http HttpClient
   * @param client The client to be updated
   * @param firstName New First name
   * @param lastName New last name
   * @param serial New serial
   */
  public static updateClient(http: HttpClient, client: Client, firstName: string, lastName: string, serial: string): Client {
    let body = {
      id: client.id,
      serialNumber: serial,
      firstName: firstName,
      lastName: lastName
    };

    http.put(Routing.clientURL + '/', body)
      .subscribe(response => {
          console.log(response);
        },
        error => {
          console.log(error);
        });
    return new Client(client.id, serial, firstName, lastName);
  }

  /**
   *
   * @param http
   * @param firstName
   */
  public static searchClientByFirstName(http: HttpClient, firstName: string): Client[] {
    let clients = [];

    http.get<Client[]>(Routing.searchClientURL + '?firstName=' + firstName)
      .subscribe(response => {
        for (let client of response) {
          clients.push(client);
        }
      }, error => {
        console.log(error);
      });

    return clients;
  }

  /**
   *
   * @param http
   * @param bookId
   * @param clientId
   * @param notes
   */
  public static createTransaction(http: HttpClient, bookId: number, clientId: number, notes: string) {

    let body = {
      bookId: bookId,
      clientId: clientId,
      //notes: notes
    };

    http.post(Routing.createTransactionURL, body)
      .subscribe(() => {
          window.location.href = '/transactions';
        },
        error => {
          confirm("An error occurred: " + error.toString());
        });
  }

  /**
   *
   * @param http
   * @param transaction
   * @param bookId
   * @param clientId
   * @param notes
   */
  public static updateTransaction(http: HttpClient, transaction: Transaction, bookId: number, clientId: number, notes: string): Transaction {

    let body = {
      id: transaction.id,
      bookId: bookId,
      clientId: clientId,
      notes: notes
    };

    http.put(Routing.transactionURL + '/', body)
      .subscribe(response => {
          console.log(response);
        },
        error => {
          console.log(error);
        });

    return new Transaction(transaction.id, bookId, clientId, notes);
  }

  /**
   *
   * @param http
   */
  public static getTransactions(http: HttpClient): Transaction[] {
    let transactions = [];

    http.get(Routing.transactionsListURL)
      .subscribe(response => {
        for(let transaction of response['transactions']) {
          transactions.push(transaction);
        }
      }, error => {
        console.log(error);
      });

    return transactions;
  }

  /**
   *
   * @param http
   * @param transaction
   */
  public static deleteTransaction(http: HttpClient, transaction: Transaction) {
    http.delete(Routing.transactionURL + '/' + transaction.id)
      .subscribe(response => {
        console.log(response);
      });
  }
}
