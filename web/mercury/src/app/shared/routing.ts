export class Routing {
  public static readonly booksListURL = "http://localhost:8080/books";
  public static readonly bookURL = "http://localhost:8080/book";
  public static readonly createBookURL = "http://localhost:8080/saveBook/";
  public static readonly createClientURL = "http://localhost:8080/client/";
  public static readonly clientsListURL = "http://localhost:8080/clients";
  public static readonly clientURL = "http://localhost:8080/client";
  public static readonly searchBookURL = "http://localhost:8080/searchBook";
  public static readonly searchClientURL = "http://localhost:8080/client";
  public static readonly createTransactionURL = "http://localhost:8080/transaction/";
  public static readonly transactionURL = "http://localhost:8080/transaction";
  public static readonly transactionsListURL = "http://localhost:8080/transactions";
}
