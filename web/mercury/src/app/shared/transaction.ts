export class Transaction{
  constructor(
    public id: number,
    public bookId: number,
    public clientId: number,
    public notes: string
  ) {}
}
