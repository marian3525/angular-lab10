import {Book} from "./book";
import {error} from "@angular/compiler/src/util";
import {ValidationException} from "./ValidationException";

export class Validator {
  static validateBook(book: Book) {
    let error_str = '';

    if(book.id < 0) {
      error_str += "ID must be greater or equal to 0, not: " + book.id;
    }

    if(book.title.length == 0) {
      error_str += "\nTitle must not be empty";
    }

    if(book.author.length == 0) {
      error_str += "\nAuthor name must not be empty";
    }

    if(book.rating < 0) {
      error_str += "\nRating must be positive";
    }

    if(error_str.length != 0)
      throw new ValidationException(error_str);
  }

  static validateBookByParams(id: number, title: string, author: string, rating: number) {
    let error_str = '';

    if(id < 0) {
      error_str += "ID must be greater or equal to 0, not: " + id;
    }

    if(title.length == 0) {
      error_str += "\nTitle must not be empty";
    }

    if(author.length == 0) {
      error_str += "\nAuthor name must not be empty";
    }

    if(rating < 0) {
      error_str += "\nRating must be positive";
    }

    if(error_str.length != 0)
      throw new ValidationException(error_str);
  }

  static validateClientByParams(id: number, firstName: string, lastName: string, serial: string) {
    let error_str = '';

    if (id < 0) {
      error_str += "ID must be greater or equal to 0, not: " + id;
    }

    if (firstName.length == 0) {
      error_str += "\nFirst name must not be empty";
    }

    if (lastName.length == 0) {
      error_str += "\nLast name must not be empty";
    }

    if (serial.length < 0) {
      error_str += "\nSerial must be positive";
    }

    if (error_str.length != 0)
      throw new ValidationException(error_str);
  }

  static validateTransactionByParams(bookId: string, clientId: string) {
    let error_str = "";

    try {
      if(Number.parseInt(bookId) < 0) {
        error_str += "The book ID must be a positive integer";
      }

      if(Number.parseInt(clientId) < 0) {
        error_str += "The client ID must be a positive integer";
      }
    }
    catch(e) {
      error_str += "Invalid number format";
    }

    if(error_str.length > 0) {
      throw new ValidationException(error_str);
    }
  }

}
