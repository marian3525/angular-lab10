import { Component, OnInit } from '@angular/core';
import {Transaction} from "../shared/transaction";
import {HttpClient} from "@angular/common/http";
import {Validator} from "../shared/validator";
import {HttpUtils} from "../shared/httpUtils";
import {Routing} from "../shared/routing";
import {Client} from "../shared/client";
import {Book} from "../shared/book";

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css']
})
export class TransactionListComponent implements OnInit {

  private transactions = [];
  private transaction: Transaction = new Transaction(0, 0, 0, "");

  private clients: Client[];
  private books: Book[];

  constructor(private http: HttpClient) {
    this.transactions = HttpUtils.getTransactions(http);
  }

  deleteTransaction(transaction) {
    HttpUtils.deleteTransaction(this.http, transaction);
    let index = this.transactions.indexOf(transaction);
    this.transactions.splice(index, 1);
  }

  openModal(transaction: Transaction) {
    this.transaction = transaction;
  }

  updateTransaction(bookId: HTMLInputElement, clientId: HTMLInputElement, notes: HTMLInputElement) {
    try {
        Validator.validateTransactionByParams(bookId.value, clientId.value);
        let newTransaction = HttpUtils.updateTransaction(this.http, this.transaction, Number.parseInt(bookId.value), Number.parseInt(clientId.value), notes.value);

        let index = this.transactions.indexOf(this.transaction);
        this.transactions[index].bookId = newTransaction.bookId;
        this.transactions[index].clientId = newTransaction.clientId;
        this.transactions[index].notes = newTransaction.notes;
    }
    catch(valException) {

    }
  }

  private getBookNameById(bookId: number) {
    for(let book of this.books) {
      if(book.id == bookId) {
        return book.title;
      }
    }
  }

  private getClientNameById(clientId: number) {
    for(let client of this.clients) {
      if(client.id == clientId) {
        return client.firstName;
      }
    }
  }

  ngOnInit() {
    this.books = HttpUtils.getBookList(this.http);
    this.clients = HttpUtils.getClientsList(this.http);
  }

}
