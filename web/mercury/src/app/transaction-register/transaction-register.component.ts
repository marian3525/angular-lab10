import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Validator} from "../shared/validator";
import {HttpUtils} from "../shared/httpUtils";
import {Book} from "../shared/book";
import {Client} from "../shared/client";

@Component({
  selector: 'app-transaction-register',
  templateUrl: './transaction-register.component.html',
  styleUrls: ['./transaction-register.component.css']
})

export class TransactionRegisterComponent implements OnInit {
  constructor(private http: HttpClient) { }

  private books: Book[];
  private clients: Client[];

  /**
   *
   * @param bookId
   * @param clientId
   * @param notes notes for the transaction, may be empty
   */
  createTransaction(bookId: HTMLInputElement, clientId: HTMLInputElement, notes: HTMLInputElement) {

    try {
      Validator.validateTransactionByParams(bookId.value, clientId.value);

      // check if the IDs exist before and pop an alert if any of them don't
      console.log("Getting books to check");

      let clients: Client[] = HttpUtils.getClientsList(this.http);

      let foundClient = false;
      let foundBook = false;

      for(let book of this.books) {
        if (book.id == Number.parseInt(bookId.value)) {
          foundBook = true;
        }
      }

      for(let client of clients) {
        if(client.id == Number.parseInt(clientId.value)) {
          foundClient = true;
        }
      }

      if(foundBook == false) {
        alert("The book with the given ID doesn't exist");
        return;
      }

      if(foundClient == false) {
        alert("The client with the given ID doesn't exist");
        return;
      }

      HttpUtils.createTransaction(this.http, Number.parseInt(bookId.value), Number.parseInt(clientId.value), notes.value);
    }
    catch(valException) {
      console.log(valException.getMessage());
    }

  }

  ngOnInit() {
     this.books = HttpUtils.getBookList(this.http);
     this.clients = HttpUtils.getClientsList(this.http);
  }

}
