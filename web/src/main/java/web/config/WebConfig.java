package web.config;

import core.service.BookServiceImpl;
import core.service.TransactionServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import core.service.ClientServiceImpl;

@Configuration
@EnableWebMvc
@ComponentScan({"web.controller", "web.converter"})
public class WebConfig {
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("http://localhost:4200", "http://localhost:8080")
                        .allowedMethods("GET", "PUT", "POST", "DELETE");
            }
        };
    }

    @Bean
    public ClientServiceImpl clientServiceImpl() {
        return new ClientServiceImpl();
    }

    @Bean
    public BookServiceImpl bookServiceImpl() {
        return new BookServiceImpl();
    }

    @Bean
    public TransactionServiceImpl transactionServiceImpl() {
        return new TransactionServiceImpl();
    }
}
