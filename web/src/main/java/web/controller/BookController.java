package web.controller;

import core.model.Book;
import core.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.converter.BookConverter;
import web.dto.BookDto;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {
    private static final Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService service;

    @Autowired
    private BookConverter converter;

    @RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
    BookDto getBooks(@PathVariable Long id) {
        log.trace("get-book: id={}", id);

        return converter.convertModelToDto(service.findById(id));
    }

    @RequestMapping(value = "/book", method = RequestMethod.GET)
    List<BookDto> filterBook(@RequestParam(value="title", required = true) String title){
        log.trace("get-book-by-title: title={}", title);

        List<Book> books = service.findByTitle(title);

        for(Book b : books){
            System.out.println(b);
        }
        List<BookDto> dtos = new ArrayList<>();

        for(Book b : books){
            dtos.add(converter.convertModelToDto(b));
        }

        return dtos;
    }

    @RequestMapping(value = "/book/", method = RequestMethod.POST)
    BookDto saveBook(@RequestBody BookDto body) {
        log.trace("save-book");

        service.save(converter.convertDtoToModel(body));

        return body;
    }

    @RequestMapping(value = "/book/", method = RequestMethod.PUT)
    BookDto updateBook(@RequestBody BookDto body) {
        log.trace("update-book");

        return converter.convertModelToDto(service.update(body.getId(), converter.convertDtoToModel(body)));
    }

    @RequestMapping(value = "/book/{id}", method = RequestMethod.DELETE)
    void deleteBook(@PathVariable Long id) {
        log.trace("delete-book");

        service.delete(id);
    }
}
