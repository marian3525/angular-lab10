package web.controller;

import core.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import web.converter.BookListConverter;
import web.dto.BookListDto;

@RestController
public class BookListController {
    private static final Logger log = LoggerFactory.getLogger(BookListController.class);

    @Autowired
    private BookService service;

    @Autowired
    private BookListConverter converter;

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    BookListDto getBooks() {
        log.trace("get-books");

        return converter.convertBooksToDto(service.findAll());
    }
}
