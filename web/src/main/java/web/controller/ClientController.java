package web.controller;

import core.model.Client;
import core.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.converter.ClientConverter;
import web.dto.ClientDto;

import java.util.ArrayList;
import java.util.List;

@RestController
class ClientController {
    private static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService service;

    @Autowired
    private ClientConverter converter;

    @RequestMapping(value = "/client/{id}", method = RequestMethod.GET)
    ClientDto getClient(@PathVariable Long id) {
        log.trace("get-client: id={}", id);

        return converter.convertModelToDto(service.findById(id));
    }

    @RequestMapping(value = "/client", method = RequestMethod.GET)
    List<ClientDto> filterClient(@RequestParam(value="firstName", required = true) String firstName){
        log.trace("get-client-by-first-name: firstName={}", firstName);

        List<Client> clients = service.findByFirstName(firstName);
        List<ClientDto> dtos = new ArrayList<>();

        for(Client c : clients){
            dtos.add(converter.convertModelToDto(c));
        }

        return dtos;
    }

    @RequestMapping(value = "/client/", method = RequestMethod.POST)
    ClientDto saveClient(@RequestBody ClientDto body) {
        log.trace("save-client");

        service.save(converter.convertDtoToModel(body));

        return body;
    }

    @RequestMapping(value = "/client/", method = RequestMethod.PUT)
    ClientDto updateClient(@RequestBody ClientDto body) {
        log.trace("update-client");

        return converter.convertModelToDto(service.update(body.getId(), converter.convertDtoToModel(body)));
    }

    @RequestMapping(value = "/client/{id}", method = RequestMethod.DELETE)
    void deleteClient(@PathVariable Long id) {
        log.trace("delete-client");

        service.delete(id);
    }
}
