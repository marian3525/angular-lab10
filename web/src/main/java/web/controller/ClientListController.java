package web.controller;

import core.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import web.converter.ClientListConverter;
import web.dto.ClientListDto;

@RestController
class ClientListController {
    private static final Logger log = LoggerFactory.getLogger(ClientListController.class);

    @Autowired
    private ClientService service;

    @Autowired
    private ClientListConverter converter;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    ClientListDto getClients() {
        log.trace("get-clients");

        return converter.convertClientsToDto(service.findAll());
    }
}
