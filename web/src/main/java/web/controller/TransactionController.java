package web.controller;

import core.model.Transaction;
import core.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import web.converter.TransactionConverter;
import web.dto.TransactionDto;

@RestController
public class TransactionController {
    private static final Logger log = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private TransactionService service;

    @Autowired
    private TransactionConverter converter;

    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.GET)
    TransactionDto getTransaction(@PathVariable Long id) {
        log.trace("get-transaction: id={}", id);

        return converter.convertModelToDto(service.findById(id));
    }

    @RequestMapping(value = "/transaction/", method = RequestMethod.POST)
    TransactionDto saveTransaction(@RequestBody TransactionDto body) {
        log.trace("save-transaction");

        service.save(converter.convertDtoToModel(body));

        return body;
    }

    @RequestMapping(value = "/transaction/", method = RequestMethod.PUT)
    TransactionDto updateTransaction(@RequestBody TransactionDto body) {
        log.trace("update-transaction");

        return converter.convertModelToDto(service.update(body.getId(), converter.convertDtoToModel(body)));
    }

    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.DELETE)
    void deleteTransaction(@PathVariable Long id) {
        log.trace("delete-transaction");

        service.delete(id);
    }
}
