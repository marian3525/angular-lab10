package web.controller;

import core.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import web.converter.TransactionListConverter;
import web.dto.TransactionListDto;

@RestController
public class TransactionListController {
    private static final Logger log = LoggerFactory.getLogger(TransactionListController.class);

    @Autowired
    private TransactionService service;

    @Autowired
    private TransactionListConverter converter;

    @RequestMapping(value = "/transactions", method = RequestMethod.GET)
    TransactionListDto getTransactions() {
        log.trace("get-transactions");

        return converter.convertClientsToDto(service.findAll());
    }
}
