package web.converter;

import core.model.Book;
import org.springframework.stereotype.Component;
import web.dto.BookDto;

@Component
public class BookConverter extends BaseConverter<Book, BookDto> {

    @Override
    public Book convertDtoToModel(BookDto dto) {
        Book book = Book.builder()
                .title(dto.getTitle())
                .author(dto.getAuthor())
                .rating(dto.getRating())
                .build();

        book.setId(dto.getId());

        return book;
    }

    @Override
    public BookDto convertModelToDto(Book book) {
        if(book != null) {
            BookDto dto = BookDto.builder()
                    .id(book.getId())
                    .title(book.getTitle())
                    .author(book.getAuthor())
                    .rating(book.getRating())
                    .build();

            dto.setId(book.getId());

            return dto;
        }

        return null;
    }
}
