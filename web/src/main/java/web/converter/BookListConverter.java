package web.converter;

import core.model.Book;
import org.springframework.stereotype.Component;
import web.dto.BookDto;
import web.dto.BookListDto;


import java.util.LinkedList;
import java.util.List;

@Component
public class BookListConverter {
    public BookListDto convertBooksToDto(List<Book> books) {
        BookListDto bookDto = new BookListDto();

        List<BookDto> list = new LinkedList<>();

        for (Book book : books) {
            BookDto dto = BookDto.builder()
                    .id(book.getId())
                    .title(book.getTitle())
                    .author(book.getAuthor())
                    .rating(book.getRating())
                    .build();
            list.add(dto);
        }
        bookDto.setBooks(list);
        return bookDto;
    }
}
