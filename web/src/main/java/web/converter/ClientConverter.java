package web.converter;

import web.dto.ClientDto;
import core.model.Client;
import org.springframework.stereotype.Component;
import web.dto.ClientListDto;

import java.util.LinkedList;
import java.util.List;

@Component
public class ClientConverter extends BaseConverter<Client, ClientDto> {
    @Override
    public Client convertDtoToModel(ClientDto dto) {
        Client client = Client.builder()
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .serialNumber(dto.getSerialNumber())
                .build();

        client.setId(dto.getId());

        return client;
    }

    @Override
    public ClientDto convertModelToDto(Client client) {
        if(null != client) {
            ClientDto dto = ClientDto.builder()
                    .id(client.getId())
                    .firstName(client.getFirstName())
                    .lastName(client.getLastName())
                    .serialNumber(client.getSerialNumber())
                    .build();

            dto.setId(client.getId());

            return dto;
        }

        return null;
    }
}
