package web.converter;

import core.model.Client;
import org.springframework.stereotype.Component;
import web.dto.ClientDto;
import web.dto.ClientListDto;

import java.util.LinkedList;
import java.util.List;

@Component
public class ClientListConverter {
    public ClientListDto convertClientsToDto(List<Client> clients) {
        ClientListDto cldto = new ClientListDto();

        List<ClientDto> list = new LinkedList<>();

        for (Client client : clients) {
            ClientDto dto = ClientDto.builder()
                    .id(client.getId())
                    .firstName(client.getFirstName())
                    .lastName(client.getLastName())
                    .serialNumber(client.getSerialNumber())
                    .build();
            list.add(dto);
        }
        cldto.setClients(list);
        return cldto;
    }
}
