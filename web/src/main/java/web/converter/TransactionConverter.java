package web.converter;

import core.model.Transaction;
import org.springframework.stereotype.Component;
import web.dto.TransactionDto;

@Component
public class TransactionConverter extends BaseConverter<Transaction, TransactionDto> {
    @Override
    public Transaction convertDtoToModel(TransactionDto dto) {
        Transaction transaction = Transaction.builder()
                .bookId(dto.getBookId())
                .clientId(dto.getClientId())
                .notes(dto.getNotes())
                .build();

        transaction.setId(dto.getId());

        return transaction;
    }

    @Override
    public TransactionDto convertModelToDto(Transaction transaction) {
        if(null != transaction) {
            TransactionDto dto = TransactionDto.builder()
                    .id(transaction.getId())
                    .bookId(transaction.getBookId())
                    .clientId(transaction.getClientId())
                    .notes(transaction.getNotes())
                    .build();

            dto.setId(transaction.getId());

            return dto;
        }

        return null;
    }
}
