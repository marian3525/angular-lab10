package web.converter;

import core.model.Transaction;
import org.springframework.stereotype.Component;
import web.dto.TransactionDto;
import web.dto.TransactionListDto;

import java.util.LinkedList;
import java.util.List;

@Component
public class TransactionListConverter {
    public TransactionListDto convertClientsToDto(List<Transaction> transactions) {

        TransactionListDto tdto = new TransactionListDto();
        List<TransactionDto> list = new LinkedList<>();

        for (Transaction t : transactions) {
            TransactionDto dto = TransactionDto.builder()
                    .id(t.getId())
                    .bookId(t.getBookId())
                    .clientId(t.getClientId())
                    .build();
            list.add(dto);
        }

        tdto.setTransactions(list);

        return tdto;
    }
}
