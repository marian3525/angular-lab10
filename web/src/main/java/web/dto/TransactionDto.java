package web.dto;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString(callSuper = true)
@Builder
public class TransactionDto extends BaseDto {
    private Long id;
    private Long bookId;
    private Long clientId;
    private String notes;
}
